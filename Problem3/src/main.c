/*
 * Description:
 */

#include <stdio.h>
#include <stdlib.h>

int getBetweenNumber(int a, int b) {
	if (abs(a-b)<2) {
		return 0;
	}
	if (a > b) {
		return a - b - 1;
	} else {
		return b - a - 1;
	}
}

void printBetweenNumbers(int a, int b) {
	int bnum = getBetweenNumber(a, b);
	if (bnum == 0) {
		printf("The numbers between %d and %d ", a, b);
		printf("is no number between the two input numbers.\n");
		return;
	}
	int startnum = a > b ? b : a;
	int endnum = a > b ? a : b;
	printf("The numbers between %d and %d are ", a, b);
	for (int i=startnum+1; i<endnum; i++) {
		printf("%d ", i);
	}
	printf("and there are %d numbers between %d and %d.\n", bnum, a, b);
}
void printAverage(int a, int b) {
	double avg = 0;
	int bnum = getBetweenNumber(a, b);
	if (bnum == 0) {
		printf("Can't calculate the average because there is no number between the two input numbers.\n");
		return;
	}
	avg = (a + b) / 2;
	printf("The average of numbers is %.1f\n", avg);
}

int main(void) {
	int a, b = 0;
	printf("Enter two number\n");
	scanf("%d %d", &a, &b);
	printBetweenNumbers(a, b);
	printAverage(a, b);
	return EXIT_SUCCESS;
}

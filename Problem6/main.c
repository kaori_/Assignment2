#include <stdio.h>
#include <stdlib.h>

int getBetweenNumber(int a, int b) {
    if (abs(a-b)<2) {
        return 0;
    }
    if (a > b) {
        return a - b - 1;
    } else {
        return b - a - 1;
    }
}

int checkPrimeNumber(int num) {
    if (num <= 0) {
        return 0;
    }
    if (num == 0) {
        return 1;
    }
    for (int i=2; i<num; i++) {
        if (num%i == 0) {
            return 0;
        }
    }
    return 1;
}

int getPrimeNumbers(int a, int b, int* pnums) {
    int startnum = a > b ? b : a;
    int endnum = a > b ? a : b;
    int i = 0;
    for (int j=startnum+1; j<endnum; j++) {
        if (checkPrimeNumber(j) == 1) {
            pnums[i] = j;
            i++;
        }
    }
    return i;
}

int main() {
    int a, b, pcount = 0;
    size_t bcount = 0;
    int* primeNumbers = NULL;

    printf("Input two numbers:\n");
    scanf("%d %d", &a, &b);
    bcount = (size_t)getBetweenNumber(a, b);
    if (bcount != 0) {
        primeNumbers = malloc(sizeof(int)*bcount);

        pcount = getPrimeNumbers(a, b, primeNumbers);
        if (pcount == 0) {
            printf("There is no prime number between %d and %d\n", a, b);
        } else {
            printf("Prime numbers are ");
            for (int i=0; i<pcount; i++) {
                printf("%d ", primeNumbers[i]);
            }
            printf("\n");
        }

        free(primeNumbers);
    } else {
        printf("There is no number between %d and %d", a, b);
    }

    return 0;
}

#include <stdio.h>

void printStar ( int num ) {
    if (num > 0) {
        for(int i=0; i<num; i++) {
            printf("*");
        }
        printf("\n");
    }
}

void printShape ( int num ) {
    for (int i=1; i<=num; i++) {
        printStar(i);
    }
}

int main() {
    int numberOfStars = 0;
    printf("Input a number of stars\n");
    scanf("%d", &numberOfStars);
    printShape(numberOfStars);

    return 0;
}
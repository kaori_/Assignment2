#include <stdio.h>

void swapNumbers(int* a, int* b) {
    int c = 0;
    c = *a;
    *a = *b;
    *b = c;
}

int main() {
    int a, b = 0;
    printf("We print the sum of the number’s digits.\n");
    printf("Input two numbers:\n");
    scanf("%d %d", &a, &b);
    printf("a = %d, b = %d\n", a, b);
    swapNumbers(&a, &b);
    printf("Swapped numbers:\n");
    printf("a = %d, b = %d\n", a, b);

    return 0;
}
#include <stdio.h>
#include <stdlib.h>

int checkOdd(int num) {
    if (num%2 == 0) {
        return 1;
    } else {
        return 0;
    }
}

int checkDividend(int num, int dividend) {
    if (num%dividend == 0) {
        return 1;
    } else {
        return 0;
    }
}

int checkPrimeNumber(int num) {
    if (num <= 0) {
        return 0;
    }
    if (num == 0) {
        return 1;
    }
    for (int i=2; i<num; i++) {
        if (num%i == 0) {
            return 0;
        }
    }
    return 1;
}

int main() {
    int number = 0;

    printf("Input a number:\n");
    scanf("%d", &number);

    if (checkOdd(number) == 1) {
        printf("%d is odd\n", number);
    } else {
        printf("%d is even\n", number);
    }

    if (checkDividend(number, 3) == 1) {
        printf("%d is dividend of 3\n", number);
    } else {
        printf("%d is NOT dividend of 3\n", number);
    }

    if (checkDividend(number, 7) == 1) {
        printf("%d is dividend of 7\n", number);
    } else {
        printf("%d is NOT dividend of 7\n", number);
    }

    if (checkPrimeNumber(number) == 1) {
        printf("%d is a prime number\n", number);
    } else {
        printf("%d is NOT a prime number\n", number);
    }

    return 0;
}

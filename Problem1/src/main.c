/*
 * Description:
 */

#include <stdio.h>
#include <stdlib.h>

double factorial(int num) {
	double ans = 1.0;
	for (int i=num; i>0; i--) {
		ans *= i;
	}
	return ans;
}

double combination(int a, int b) {
	double ans = 0;
	ans = (factorial(a)/(factorial(a-b)*factorial(b)));

	return ans;
}

int main(void) {
	int a, b = 0;
	printf("Enter two number\n");
	scanf("%d %d", &a, &b);
	printf("%dC%d = %9.0f\n", a, b, combination(a, b));

	return EXIT_SUCCESS;
}

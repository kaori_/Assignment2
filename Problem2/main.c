#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int getNumberOfCircles(int a, int b, int r) {
    double areaRect, areaCircle = 0;
    areaRect = (double)(a * b);
    areaCircle = r * r * M_PI;

    return (int)(areaRect/areaCircle);
}

int main() {
    int a, b, r = 0;

    printf("Input two sides of rectangle:\n");
    scanf("%d %d", &a, &b);
    printf("Input radius of circle:\n");
    scanf("%d", &r);

    if (a<=0 || b<=0 || r<=0) {
        printf("Inputs is wrong.");
        exit(1);
    }
    printf("%d circles can be entered.", getNumberOfCircles(a, b, r));

    return 0;
}
